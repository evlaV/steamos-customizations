#!/bin/bash
# -*- mode: sh; indent-tabs-mode: nil; sh-basic-offset: 4; -*-
# vim: et sts=4 sw=4

#  SPDX-License-Identifier: LGPL-2.1+
#
#  Copyright © 2021 Collabora Ltd.
#  Copyright © 2021 Valve Corporation.
#
#  This file is part of steamos-customizations.
#
#  steamos-customizations is free software; you can redistribute it and/or
#  modify it under the terms of the GNU Lesser General Public License as
#  published by the Free Software Foundation; either version 2.1 of the License,
#  or (at your option) any later version.

set -e
set -u

current_image=
target_image=

set_current_image ()
{
    local name=

    if [ -n "$current_image" ]
    then
        return
    fi

    name=$(steamos-bootconf this-image)
    case $name in
        A|B|dev)
            true
            ;;
        *)
            # unexpected image: pick highest prio well-known name as primary
            while read valid slot x
            do
                case $slot in
                    A|B)
                        name=$slot
                    ;;
                esac
            done < <(steamos-bootconf list-images)
            ;;
    esac

    current_image=$name
}

set_target_image ()
{
    case ${1:-} in
        self)
            set_current_image
            target_image="$current_image"
            ;;
        A|B|dev)
            target_image="$1"
            ;;
        *)
            echo "Invalid target image $1" >&2
            exit 22
    esac
}

ensure_exists()
{
    steamos-bootconf config --image "${1}" || steamos-bootconf create --image "${1}" ||:
}

case ${1:-} in
get-primary)
    set_current_image

    image=$(steamos-bootconf selected-image)

    if [ "$image" = dev ]
    then
        image=$current_image
    fi

    if [ -n "$image" ]
    then
        echo "$image"
    elif [ -n "$current_image" ]
    then
        echo "$current_image"
    else
        exit 1
    fi
    ;;

set-primary)
    set_target_image "${2:-}"
    ensure_exists "${target_image}"
    steamos-bootconf --image "$target_image" set-mode reboot
    ;;

get-state)
    tries=
    invalid=

    set_target_image "${2:-}"

    while read key val x
    do
        case $key in
            boot-attempts:) tries=$val; ;;
            image-invalid:) invalid=$val; ;;
        esac
    done < <(steamos-bootconf --image "$target_image" \
                              config --get boot-attempts --get image-invalid)

    # If we retrieved no data:
    if [ -z "$tries" ] || [ -z "$invalid" ]
    then
        echo "Missing boot configuration for $target_image" >&2
        exit 1
    fi

    # 1 or more unsuccessful/incomplete boot attempts
    # or the image is flagged invalid for some reason:
    if [ ${tries:-} -ge 1 ] || [ ${invalid:-0} -gt 0 ]
    then
        echo bad
    else
        echo good
    fi
    ;;

set-state)
    set_target_image "${2:-}"
    ensure_exists "${target_image}"

    case ${3:-} in
        good)
            # clears boot-other and update, sets boot-requested-at
            steamos-bootconf --image "$target_image" set-mode reboot \
                             --set image-invalid 0
            ;;
        bad)
            boot_to=
            # clears update, sets boot-other and boot-requested-at
            steamos-bootconf --image "$target_image" set-mode reboot-other \
                             --set image-invalid 1

            ;;
        *)
            echo "Bad image state value '${3:-}' { good; bad }" >&2
            exit 22
            ;;
    esac
    ;;

*)
    echo "${1:-(nil)}: Invalid argument" >&2
    exit 127
    ;;
esac
