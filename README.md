SteamOS Customizations
======================

You everyday commands:

    # build it all
    make DESTDIR=tmp install

    # have a look at the result
    tree tmp

Expected Make variables (for packagers):
- `DESTDIR`: Staging installation directory.


Structure
---------

Every directory is more or less like a lightweight package. It comes with its
own `Makefile` and its own `.gitignore` file.

Everything that doesn't really belong to a particular feature, or is too small
to deserve its own sub-directory, ends up in the directory `misc`.


Content
-------

 * atomic-update: Configuration and tools to perform Atomic Updates using rauc
 * chainloader: Boot configuration
 * dracut: dracut script and configurations to mount the rootfs
 * mkinitcpio: initrd configuration which handles fs mounting, overlays, etc
 * grub: GRUB2 bootloader tools and services
 * misc: Set of features
 * offload: Set of mount targets for read-write partitions
 * plymouth: Script to show logging messages on boot screen
 * settings-importer: First-boot settings importer
 * swap: Configuration to create the swap file at startup and use it afterwards


Background
----------

This package was created by merging two git repositories:
- steamos-packages
- steamos-partitions

All the git history was lost in the process. So if you're after some details,
you might want to refert to the git logs of those git repos.

